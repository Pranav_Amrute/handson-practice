1. Create a class named Venue with the following member variables / attributes (Default access) 

Data Type Variable Name 
String	 name 
String	 city 

Create another class called Main and write a main method to test the above class. 
 
Input and Output Format: 
 Refer sample input and output for formatting specifications. 
All text in bold corresponds to input and the rest corresponds to output. 

Sample Input and Output : 
Enter the venue name 
M. A. Chidambaram Stadium 
Enter the city name 
Chennai 
Venue Details : 
Venue Name : M. A. Chidambaram Stadium 
City Name : Chennai 

Solution Program :

package pranav.sme.session2.hcl;

import java.util.Scanner;

class Venue {
	String name;
	String city;

}

public class Main {

	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);

		Venue venue = new Venue();

		System.out.println("Enter The Venue Name: ");
		venue.name = scanner.nextLine();
		System.out.println("Enter The City Name: ");
		venue.city = scanner.nextLine();

		System.out.println("Venue Details :");

		System.out.println("Venue Name:" + venue.name);
		System.out.println("City Name:" + venue.city);

	}

}

O/p :


Enter The Venue Name: 
Chennai
Enter The City Name: 
M. A. Chidambaram Stadium 
Venue Details :
Venue Name:Chennai
City Name:M. A. Chidambaram Stadium 



--------------------------------------------------------------------------------------------------------------------------------------------------------------------------

2. Create a class named Player with the following member variables / attributes (Default access) 

Data Type Variable Name 
String 	name 
String 	country 
String 	skill 

Create another class named Main and write a main method to test the above class. 

Input and Output Format: 

Refer sample input and output for formatting specifications. 
All text in bold corresponds to input and the rest corresponds to output. 

Sample Input and Output : 
Enter the player name 
MS Dhoni 
Enter the country name 
India 
Enter the skill 
All Rounder 
Player Details : 
Player Name : MS Dhoni 
Country Name : India 
Skill : All Rounder 

Solution :

package pranav.sme.session2.hcl;

import java.util.Scanner;

class Player {

	String name;
	String country;
	String skill;

}

public class Main1 {

	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);

		Player player = new Player();

		System.out.println("Enter the Player name");

		player.name = scanner.nextLine();

		System.out.println("Enter the country name");

		player.country = scanner.nextLine();

		System.out.println("Enter the skill");

		player.skill = scanner.nextLine();

		System.out.println("Player Details");

		System.out.println("Player Name:" + player.name);
		System.out.println("Country Name:" + player.country);
		System.out.println("Skill:" + player.skill);

	}

}


O/p :

Enter the Player name
M. S. Dhoni
Enter the country name
India
Enter the skill
Wicket Keeper Batsman
Player Details
Player Name:M. S. Dhoni
Country Name:India
Skill:Wicket Keeper Batsman


--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


3. Create a class named Delivery with the following public member variables / attributes 

Data Type Variable Name 
Long		 over 
Long		 ball 
Long 		runs 
String	 batsman 
String 	bowler 
String 	nonStriker 
 
Include a method in the class named displayDeliveryDetails(). In this method, display the details of the 
delivery in the format shown in the sample output. This method does not accept any arguments and its 
return type is void. 
Create another class called Main and write a main method to test the above class. 
 
Input and Output Format: 
 Refer sample input and output for formatting specifications. 
All text in bold corresponds to input and the rest corresponds to output. 
 
Sample Input and Output : 
Enter the over 
1 
Enter the ball 
1 
Enter the runs 
4 
Enter the batsman name 
MS Dhoni 
Enter the bowler name 
Dale steyn 
Enter the nonStriker name 
Suresh Raina 
Delivery Details : 
Over : 1 
Ball : 1 
Runs : 4 
Batsman : MS Dhoni 
Bowler : Dale steyn 
NonStriker : Suresh Raina 


Solution :


package pranav.sme.session2.hcl;

import java.util.Scanner;

class Delivery {

	public long over;
	public long ball;
	public long runs;
	public String batsman;
	public String bowler;
	public String nonStriker;

	void displayDeliveryDetails() {

		System.out.println("Delivery Details:");
		System.out.println("Over : " + over);
		System.out.println("Ball : " + ball);
		System.out.println("Runs : " + runs);
		System.out.println("Batsman : " + batsman);
		System.out.println("Bowler : " + bowler);
		System.out.println("NonStriker : " + nonStriker);

	}
}

public class Main2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner scanner = new Scanner(System.in);

		Delivery delivery = new Delivery();

		System.out.println("Enter the Over: ");
		delivery.over = scanner.nextLong();

		System.out.println("Enter the ball: ");
		delivery.ball = scanner.nextLong();

		System.out.println("Enter the runs: ");
		delivery.runs = scanner.nextLong();

		scanner.nextLine();

		System.out.println("Enter the batsman name");
		delivery.batsman = scanner.nextLine();

		System.out.println("Enter the bowler name");
		delivery.bowler = scanner.nextLine();

		System.out.println("Enter the nonStriker name");
		delivery.nonStriker = scanner.nextLine();

		delivery.displayDeliveryDetails();

	}

}

o/p :

Enter the Over: 
1
Enter the ball: 
2
Enter the runs: 
3
Enter the batsman name
Dhoni
Enter the bowler name
Bolt
Enter the nonStriker name
Raina
Delivery Details:
Over : 1
Ball : 2
Runs : 3
Batsman : Dhoni
Bowler : Bolt
NonStriker : Raina

--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


4. Create a class named Player with the following member variables / attributes (default access) \

Data Type Variable Name 
String	 name 
String 	country 
String 	skill 
Create another class called Main and write a main method to get the player details in a string seperated 
by comma.Use String. split() function to display the details. 

Input and Output Format: 
Refer sample input and output for formatting specifications. 
All text in bold corresponds to input and the rest corresponds to output. 

Sample Input and Output : 
Enter the player details 
MS Dhoni,India,All Rounder 
Player Details 
Player Name : MS Dhoni 
Country Name : India 
Skill : All Rounder 


Solution :


package pranav.sme.session2.hcl;

import java.util.Scanner;

class Player1 {
    String name;
    String country;
    String skill;

   
    public Player1(String details) {
        String[] detailsArr = details.split(",");
        if (detailsArr.length == 3) {
            name = detailsArr[0];
            country = detailsArr[1];
            skill = detailsArr[2];
        }
    }
}

public class Main3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter the player details");
        String playerDetails = scanner.nextLine();

        Player1 player = new Player1(playerDetails);

        System.out.println("Player Details");
        System.out.println("Player Name : " + player.name);
        System.out.println("Country Name : " + player.country);
        System.out.println("Skill : " + player.skill);
    }
}


O/p :

Enter the player details
Rohit Sharma, India, Batsman
Player Details
Player Name : Rohit Sharma
Country Name :  India
Skill :  Batsman


----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



5. Create a class named Venue with the following private member variables / attributes 
String name 
String city 
Include appropriate getters and setters. 
[Naming Convention: 
getters : getName getCity... 
setters : setName setCity...] 
Create another class and write a main method to test the above class. In the main method, get the 
choice from the user and update the corresponding venue details. 
 


Solution :


package pranav.sme.session2.hcl;
import java.util.Scanner;

class Venue1 {
     String name;
     String city;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCity() {
        return city;
    }

	public void setCity(String city) {
        this.city = city;
    }
}

public class Main4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Venue1 venue = new Venue1();

        System.out.println("Enter the venue name");
        venue.setName(scanner.nextLine());

        System.out.println("Enter the city name");
        venue.setCity(scanner.nextLine());

        System.out.println("Venue Details");
        System.out.println("Venue Name : " + venue.getName());
        System.out.println("City Name : " + venue.getCity());

       
        boolean exit=true;
        while (true) {
            System.out.println("Verify and Update Venue Details");
            System.out.println("Menu");
            System.out.println("1. Update Venue Name");
            System.out.println("2. Update City Name");
            System.out.println("3. All informations Correct/Exit");
            System.out.println("Type 1 or 2 or 3");

            int choice = scanner.nextInt();
            scanner.nextLine(); 

            switch (choice) {
                case 1:
                    System.out.println("Enter the venue name");
                    venue.setName(scanner.nextLine());
                    break;
                case 2:
                    System.out.println("Enter the city name");
                    venue.setCity(scanner.nextLine());
                    break;
                case 3:
                    exit=false;
                    break;
                default:
                    System.out.println("Invalid choice. Try again.");
            }

            System.out.println("Venue Details");
            System.out.println("Venue Name : " + venue.getName());
            System.out.println("City Name : " + venue.getCity());
        }
    }
}


O/p :


Enter the venue name
Wankhade Stadium 
Enter the city name
Mumbai
Venue Details
Venue Name : Wankhade Stadium 
City Name : Mumbai
Verify and Update Venue Details
Menu
1. Update Venue Name
2. Update City Name
3. All informations Correct/Exit
Type 1 or 2 or 3
1
Enter the venue name
Chinnaswami Stadium 
Venue Details
Venue Name : Chinnaswami Stadium 
City Name : Mumbai
Verify and Update Venue Details
Menu
1. Update Venue Name
2. Update City Name
3. All informations Correct/Exit
Type 1 or 2 or 3
2
Enter the city name
Chennai
Venue Details
Venue Name : Chinnaswami Stadium 
City Name : Chennai
Verify and Update Venue Details
Menu
1. Update Venue Name
2. Update City Name
3. All informations Correct/Exit
Type 1 or 2 or 3
3
Venue Details
Venue Name : Chinnaswami Stadium 
City Name : Chennai
Verify and Update Venue Details
Menu
1. Update Venue Name
2. Update City Name
3. All informations Correct/Exit
Type 1 or 2 or 3


------------------------------------------------------------------------------------------------------------------------------------------------------


















